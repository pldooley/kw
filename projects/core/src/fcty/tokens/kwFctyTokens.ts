/**********************************************************************
 *
 * kw/class/tokens/kwFctyTokens.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import {kw }                    from '../../kw';
import {kwtBs }                 from '../../class/Bs/kwtBs';
import {kwTokens }              from '../../class/tokens/kwTokens';
import {kwTokenType }           from '../../class/token/kwTokenType';
//@formatter:on


const sProp: string = 'tokens';


export class kwFctyTokens
{
    static create(info: kwtBs): kwTokens
    {
        //console.log('kwsfctyTokens::create() called');

        if (kw.isNull(info))
        {
            console.error('kwsfctyTokens::create() info is invalid');
            return;
        }
        //console.info('kwsfctyTokens::create() info is ', info);

        const list: kwTokenType[] = info[sProp];
        if (!kw.isArray(list))
        {
            console.error('kwsfctyTokens()::create() list is invalid.');
            return;
        }
        //console.info('kwsfctyTokens()::create() list is ', list);

        const tokens: kwTokens = new kwTokens(list);
        if (!tokens.init())
        {
            console.error('kwsfctyTokens::create() error initializing tokens.');
            return;
        }
        //console.info('kwsfctyTokens::create() tokens is ', tokens);

        return tokens;
    }

}

