/**********************************************************************
 *
 * kw/ctrl/kwCtrlPropStatObj
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off

import {kwCtrlPropStat}   from './kwCtrlPropStat';
import {kwLog }            from '../kwLog';
import {kwSt }             from '../stat/kwSt';
import {kwStObj }          from '../stat/kwStObj';
import {kwStStat}          from '../stat/kwStStat';



export abstract class kwCtrlPropStatObj extends kwCtrlPropStat
{


    protected constructor(
        src: kwSt,
        dst: kwStObj,
        sProp: string,
        nStat: number,
        srvcStat: kwStStat  )
    {
        super(
            src,
            dst,
            sProp,
            nStat,
            srvcStat    );

        const log: kwLog = new kwLog(this.sClass, 'constructor');
        //console.log(log.called());
    }


}
