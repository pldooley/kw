/**********************************************************************
 *
 * kw/ctrl/kwCtrlHttpMap.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off

import {kwCtrlHttp }       from './kwCtrlHttp';
import {kwHttpMsg }        from '../http/kwHttpMsg';
import {kwLog }            from '../kwLog';
import {kwStMap }          from '../stat/kwStMap';
import {kwStMsg }          from '../stat/kwStMsg';
//@formatter:on

export abstract class kwCtrlHttpMap extends kwCtrlHttp
{

    protected constructor(
        src: kwStMsg,
        dst: kwStMap,
        srvcHttp: kwHttpMsg )
    {
        super(src, dst, srvcHttp);

        const log: kwLog = new kwLog(this.sClass, 'constructor');
        //console.log(log.called());
    }

}
