/**********************************************************************
 *
 * kw/options/kwOptionsType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import {kwAct }                 from '../act/kwAct';
import {kwAjax }                from '../ajax/kwAjax';
import {kwMode }                from '../mode/kwMode';
import {kwParam }               from '../param/kwParam';
import {kwSrvcs }               from '../srvcs/kwSrvcs';
import {kwTokens }              from '../tokens/kwTokens';
//@formatter:on


export class kwOptionsType
{
    act: kwAct;
    ajax: kwAjax;
    data: object;
    mode: kwMode;
    params?: any;
    srvcs: kwSrvcs;
    tokens: kwTokens;
}
