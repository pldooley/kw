/**********************************************************************
 *
 * kw/class/aws/kwAWSType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import {kwActsType }        from '../acts/kwActsType';
//@formatter:on


export class kwAWSType
{
    sBody: string;
    sBucket: string;
    sKwy: string;
}
