/**********************************************************************
 *
 * kw/options/kwPageType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import {kwFctyMsg}      from '../../fcty/msg/kwFctyMsg';
import {kwStArr}        from '../../stat/kwStArr';
import {kwStMsg}        from '../../stat/kwStMsg';
//@formatter:on


export class kwPageType
{
    srvcData: kwStArr;
    srvcfcty: kwFctyMsg;
    srvcMsg: kwStMsg;
}

