/**********************************************************************
 *
 * kw/class/page/kwPageAll.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

//@formatter:off
import {kw}                 from '../../kw';
import {kwLog}              from '../../kwLog';
import {kwMsg}              from '../msg/kwMsg';
import {kwPage}             from './kwPage';
import {kwPageEnum}         from '../page/kwPageEnum';
import {kwPageType}         from '../page/kwPageType';

const nTYPE: kwPageEnum = kwPageEnum.all;


export class kwPageAll extends kwPage
{

    public constructor(
        type: kwPageType    )
    {
        super(
            nTYPE,
            type    );

        const log: kwLog = new kwLog(this.sClass, 'constructor');
        //console.log(log.called());
    }


//@formatter:on

    protected createMsg(fltr?: any): boolean
    {
        const log: kwLog = new kwLog(this.sClass, 'createMsg');
        //console.log(log.called());


        if (kw.isNull(this.srvcfcty))
        {
            console.error(log.invalid('srvcfcty'));
            return false;
        }


        if (kw.isNull(this.srvcMsg))
        {
            console.error(log.invalid('srvcMsg'));
            return false;
        }


        const msg: kwMsg = this.srvcfcty.all();
        if (!kw.isValid(msg))
        {
            console.error(log.errCreate('msg'));
            return false;
        }
        //console.info(log.isObj('msg'), msg);

        this.srvcMsg.val = msg;

        return true;
    }

}
