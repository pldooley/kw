
/**********************************************************************
 *
 * kw/class/page/kwPage.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import {kw }                from '../../kw';
import {kwFctyMsg}          from '../../fcty/msg/kwFctyMsg';
import {kwLog}              from '../../kwLog';
import {kwPageEnum }        from './kwPageEnum';
import {kwPageType}         from '../page/kwPageType';
import {kwStArr}            from '../../stat/kwStArr';
import {kwStMsg}            from '../../stat/kwStMsg';
//@formatter:on

export abstract class kwPage
{
    protected sClass: string = this.constructor.name;


    protected srvcData: kwStArr;
    protected srvcfcty: kwFctyMsg;
    protected srvcMsg: kwStMsg;


    protected constructor(
        private nType: kwPageEnum,
        private type:  kwPageType  )
    {
        const log: kwLog = new kwLog(this.sClass, 'constructor');
        //console.log(log.called());
    }

    protected abstract createMsg(fltr?: any): boolean;


    public init(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, 'init');
        //console.log(log.called());


        if (kw.isNull(this.type))
        {
            console.error(log.invalid('type'));
            return false;
        }
        //console.info(log.isObj('type'), this.type);


        this.srvcData = this.type.srvcData;
        if (kw.isNull(this.srvcData))
        {
            console.error(log.invalid('srvcData'));
            return false;
        }
        //console.info(log.isObj('srvcData'), this.srvcData);


        this.srvcfcty = this.type.srvcfcty;
        if (kw.isNull(this.srvcfcty))
        {
            console.error(log.invalid('srvcfcty'));
            return false;
        }
        //console.info(log.isObj('srvcfcty'), this.srvcfcty);


        this.srvcMsg = this.type.srvcMsg;
        if (kw.isNull(this.srvcMsg))
        {
            console.error(log.invalid('srvcMsg'));
            return false;
        }
        //console.info(log.isObj('srvcMsg'), this.srvcMsg);


        if (!this.createMsg())
        {
            console.error(log.errCreate('msg'));
            return false;
        }

        return true;
    }

    public destroy(): void
    {
        const log: kwLog = new kwLog(this.sClass, 'destroy');
        //console.log(log.called());
    }


    toString(): string
    {
        return this.constructor.name;
    }

    static is(obj: object): boolean
    {
        return kw.is(obj, kwPage)
    }

}
