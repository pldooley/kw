/**********************************************************************
 *
 * kw/msg/kwMsgType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off

import {kwApi }             from '../api/kwApi';
import {kwSrvcs }           from '../srvcs/kwSrvcs';
import {kwTokens }          from '../tokens/kwTokens';
//@formatter:on


export class kwMsgType
{
    api: kwApi;
    data: object;
    params?: any;
    srvcs: kwSrvcs;
    tokens: kwTokens;
}

