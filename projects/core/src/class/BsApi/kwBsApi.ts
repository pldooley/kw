/**********************************************************************
 *
 * kw/class/BsApi/kwBsApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import {kw }                       from '../../kw';
import {kwApi }                    from '../api/kwApi';
import {kwApiType }                from '../api/kwApiType';
import {kwBs }                     from '../Bs/kwBs';
//@formatter:on


export class kwBsApi extends kwApi
{
    constructor(
        private BS: kwBs,
        type: kwApiType )
    {
        super(type)
        //console.log('kwBsApi::constructor() called');
    }

    static is(obj: object): boolean
    {
        return kw.is(obj, kwBsApi)
    }

}

