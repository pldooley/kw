/**********************************************************************
 *
 * kw/class/url/kwtUrl.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import {kwAjax }       from '../ajax/kwAjax';
import {kwSrvcs }      from '../srvcs/kwSrvcs';
//@formatter:on


export class kwtUrl
{
    ajax: kwAjax;
    params?: object[];
    srvcs: kwSrvcs;
}
