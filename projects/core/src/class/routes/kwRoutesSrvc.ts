/**********************************************************************
 *
 * kw/class/route/kwRoutesSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import {kw }                    from '../../kw';
import {kwRoute }              from '../route/kwRoute';
import {kwRouteType }          from '../route/kwRouteType';
//@formatter:on


export class kwRoutesSrvc
{

    static isType(obj: object): boolean
    {
        return false;
    }

    static in(nVal: number): boolean
    {
        return false
    }

    static toEnum(sVal: string): number
    {
        return -1;
    }
}

