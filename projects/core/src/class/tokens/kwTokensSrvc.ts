/**********************************************************************
 *
 * kw/class/token/kwTokensSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import {kw}                    from '../../kw';
import {kwToken}              from '../token/kwToken';
import {kwTokenType}          from '../token/kwTokenType';
//@formatter:on


export class kwTokensSrvc
{

    static isType(obj: object): boolean
    {
        return false;
    }

    static in(nVal: number): boolean
    {
        return false
    }

    static toEnum(sVal: string): number
    {
        return -1;
    }
}

