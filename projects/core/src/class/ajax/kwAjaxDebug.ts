/**********************************************************************
 *
 * kw/class/ajax/kwAjaxDebug.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import {kwAjax }                from '../ajax/kwAjax';
import {kwAjaxEnum }            from '../ajax/kwAjaxEnum';
import {kwAjaxType }            from '../ajax/kwAjaxType';
//@formatter:on


export class kwAjaxDebug extends kwAjax
{
    constructor(private data: kwAjaxType)
    {
        super(kwAjaxEnum.debug, data);
        //console.log('kwAjaxDebug::constructor() is called.');
    }
}

