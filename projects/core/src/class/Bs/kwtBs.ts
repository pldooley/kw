/**********************************************************************
 *
 * kw/class/api/kwtBs.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import {kwtAutoLogin }         from '../autoLogin/kwtAutoLogin';
import {kwtAttr }              from '../attr/kwtAttr';
import {kwtCred }              from '../cred/kwtCred';
import {kwtDisp }              from '../disp/kwtDisp';
import {kwtRedirect }          from '../redirect/kwtRedirect';
import {kwtRoutes }            from '../routes/kwtRoutes';
import {kwtSrvc }              from '../srvc/kwtSrvc';
import {kwTrace }              from '../trace/kwTrace';
//@formatter:on


export class kwtBs
{
    apis: object;
    attrs: kwtAttr[];
    autoLogin: kwtAutoLogin;
    credentials: kwtCred;
    display: kwtDisp;
    mdls: object;
    sMode: string;
    redirect: kwtRedirect;
    routes: kwtRoutes;
    srvcs: kwtSrvc[];
    trace: kwTrace;
}
