/**********************************************************************
 *
 * kwUi/class/inits/kwtInits.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
//@formatter:off
import {kwAttrs}    from '@kunstwerk/core';
import {kwDisp}     from '@kunstwerk/core';

//@formatter:on


export class kwtInits
{
    attrs: kwAttrs;
    disp: kwDisp;
    inits: object;
}
