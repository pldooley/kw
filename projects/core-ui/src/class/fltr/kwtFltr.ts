/**********************************************************************
 *
 * kwUi/class/fltr/kwtFltr.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
//@formatter:off
import {kwAttrs}    from '@kunstwerk/core';
import {kwDisp}     from '@kunstwerk/core';
import {kwView}     from '@kunstwerk/core';

//@formatter:on


export class kwtFltr
{
    attrs: kwAttrs;
    disp: kwDisp;
    inits: object;
    view: kwView;
}
