/**********************************************************************
 *
 * kwUi/ctrl/kwUiCtrlPage.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
//@formatter:off

import {kwLog}              from '@kunstwerk/core';
import {kwStArr}            from '@kunstwerk/core';
import {kwUiStInit}         from '../stat/kwUiStInit';
import {kwStObj}            from '@kunstwerk/core';
import {kwUiStView}         from '../stat/kwUiStView';
import {kwUiCtrlPage}       from './kwUiCtrlPage';


export abstract class kwUiCtrlPageSmpl extends kwUiCtrlPage
{

    protected constructor(
        srvcAttrs: kwStArr,
        srvcDisp: kwStObj,
        srvcInit: kwUiStInit,
        srvcView: kwUiStView )
    {
        super(
            srvcAttrs,
            srvcDisp,
            srvcInit,
            srvcView    );

        const log: kwLog = new kwLog(this.sClass, 'constructor');
        //console.log(log.called());
    }

//@formatter:on

    protected initPage(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, 'initPage');
        //console.log(log.called());

        return true;
    }

    protected loadData(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, 'loadData');
        //console.log(log.called());
        return true;
    }

    protected parseData(data: any)
    {
        const log: kwLog = new kwLog(this.sClass, 'parseData');
        //console.log(log.called());
    }

    protected subscribeData(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, 'subscribeData');
        //console.log(log.called());
        this.bSubData = true;
        return true;
    }

    protected unSubscribeData(): void
    {
        const log: kwLog = new kwLog(this.sClass, 'subscribeData');
        //console.log(log.called());
    }

}
