/**********************************************************************
 *
 * kwUi/ctrl/kwUiCtrlPageMsgUrl.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

//@formatter:off
import { ActivatedRoute }           from '@angular/router';

import { kwLog }                    from '@kunstwerk/core';
import { kwStArr }                  from '@kunstwerk/core';
import { kwUiStInit}                from '../stat/kwUiStInit';
import { kwStMsg }                  from '@kunstwerk/core';
import {kwStObj}                    from '@kunstwerk/core';
import { kwUiStView }               from '../stat/kwUiStView';
import { kwFctyMsg }                from '@kunstwerk/core';
import {kwUiCtrlPageMsg}            from './kwUiCtrlPageMsg';



export abstract class kwUiCtrlPageMsgUrl extends kwUiCtrlPageMsg
{

    constructor(
        srvcAttrs:  kwStArr,
        srvcDisp:   kwStObj,
        srvcInit:   kwUiStInit,
        srvcView:   kwUiStView,
        srvcData:   kwStArr,
        srvcFcty:   kwFctyMsg,
        srvcMsg:    kwStMsg )
    {
        super(
            srvcAttrs,
            srvcDisp,
            srvcInit,
            srvcView,
            srvcData,
            srvcFcty,
            srvcMsg     );

        const log: kwLog = new kwLog(this.sClass, 'constructor');
        //console.log(log.called());

    }

//@formatter:on


    protected unSubscribeFltr(): void
    {
        const log: kwLog = new kwLog(this.sClass, 'unSubscribeFltr');
        //console.log(log.called());
    }


}
