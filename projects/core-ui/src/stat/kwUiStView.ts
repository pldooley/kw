/**********************************************************************
 *
 * kwUi/stat/kwStView.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

//@formatter:off
import {kw}                 from '@kunstwerk/core';
import {kwLog}              from '@kunstwerk/core';
import {kwSt}               from '@kunstwerk/core';
import {kwStTrace}          from '@kunstwerk/core';


const sDATA_TYPE: string    = 'View';

//@formatter:off


export abstract class kwUiStView extends kwSt
{
    protected constructor
    (
        trace: kwStTrace,
        data?: object   )
    {
        super(
            sDATA_TYPE,
            trace,
            data);

        const log: kwLog = new kwLog(this.sClass, 'constructor');
        //console.log(log.called());
    }

    protected setValImpl(data: any): void
    {
        const log: kwLog = new kwLog(this.sClass, 'setValImpl');
        //console.log(log.called());

        if (!kw.isValid(data))
        {
            //console.info(log.empty('data'));
            this.subject.next(null);
            return;
        }


        this.traceInt(data);


        // Notify the observers
        this.subject.next(data);
    }


}
