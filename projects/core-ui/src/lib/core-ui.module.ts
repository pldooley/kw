import { NgModule } from '@angular/core';
import { CoreUIComponent } from './core-ui.component';



@NgModule({
  declarations: [CoreUIComponent],
  imports: [
  ],
  exports: [CoreUIComponent]
})
export class CoreUIModule { }
