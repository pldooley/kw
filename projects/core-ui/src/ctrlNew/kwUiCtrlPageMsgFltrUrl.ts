/**********************************************************************
 *
 * kwUi/ctrl/kwUiCtrlPageMsgUrl.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

//@formatter:off
/*
import {kw}                   from '@kunstwerk/core';
import {kwUiCtrlPageMsgFltr}  from './kwUiCtrlPageMsgFltr';
import {kwFctyMsg}            from '@kunstwerk/core';
import {kwLog}                from '@kunstwerk/core';
import {kwPageFltrUrl}        from '@kunstwerk/core';
import {kwStArr}              from '@kunstwerk/core';
import {kwStObj}              from '@kunstwerk/core';
import {kwStInit}             from '../stat/kwUiStInit';
import {kwStMsg}              from '@kunstwerk/core';
import {kwStVal}              from '@kunstwerk/core';
import {kwStView}             from '../stat/kwUiStView';



export abstract class kwUiCtrlPageMsgFltrUrl extends kwUiCtrlPageMsgFltr
{

    protected constructor(
        srvcAttrs: kwStVal,
        srvcDisp: kwStObj,
        srvcInit: kwStInit,
        srvcView: kwStView,
        srvcData: kwStArr,
        srvcFcty: kwFctyMsg,
        srvcMsg: kwStMsg  )
    {
        super(
            srvcAttrs,
            srvcDisp,
            srvcInit,
            srvcView,
            srvcData,
            srvcFcty,
            srvcMsg);

        const log: kwLog = new kwLog(this.sClass, 'constructor');
        //console.log(log.called());

        this.retrieveFltrData();
    }

    protected abstract retrieveFltrData(): void;

//@formatter:on

    protected init(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, 'init');
        //console.log(log.called());

        return super.init();
    }

    protected destroy(): void
    {
        const log: kwLog = new kwLog(this.sClass, 'destroy');
        //console.log(log.called());

        super.destroy();
    }

    protected createPage(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, 'createPage');
        //console.log(log.called());

        if (kw.isNull(this.type))
        {
            console.error(log.invalid('type'));
            return false;
        }

        this.page = new kwPageFltrUrl(this.type);
        if (!this.page.init())
        {
            console.error(log.errInit('page'));
            return false;
        }

        return true;
    }

}
*/
