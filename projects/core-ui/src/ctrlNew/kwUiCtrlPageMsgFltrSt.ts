/**********************************************************************
 *
 * kwUi/ctrl/kwUiCtrlPageMsgSt.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

//@formatter:off
/*
import {kw}                         from '@kunstwerk/core';
import {kwUiCtrlPageMsgFltr}        from './kwUiCtrlPageMsgFltr';
import {kwFctyMsg}                  from '@kunstwerk/core';
import {kwLog}                      from '@kunstwerk/core';
import {kwStArr}                    from '@kunstwerk/core';
import {kwStObj}                    from '@kunstwerk/core';
import {kwStInit}                   from '../stat/kwUiStInit';
import {kwStMsg}                    from '@kunstwerk/core';
import {kwStVal}                    from '@kunstwerk/core';
import {kwStView}                   from '../stat/kwUiStView';
import {kwPageFltrSt}               from '@kunstwerk/core';


export abstract class kwUiCtrlPageMsgFltrSt extends kwUiCtrlPageMsgFltr
{



    protected constructor(
        srvcAttrs:  kwStVal,
        srvcDisp:   kwStObj,
        srvcInit:   kwStInit,
        srvcView:   kwStView,
        srvcData:   kwStArr,
        srvcFcty:   kwFctyMsg,
        srvcMsg:    kwStMsg,
        private srvcFltr:   kwStVal)
    {
        super(
            srvcAttrs,
            srvcDisp,
            srvcInit,
            srvcView,
            srvcData,
            srvcFcty,
            srvcMsg  );

        const log: kwLog = new kwLog(this.sClass, 'constructor');
        //console.log(log.called());
    }


//@formatter:on

    protected init(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, 'init');
        //console.log(log.called());

        if (kw.isNull(this.srvcFltr))
        {
            console.error(log.invalid('srvcFltr'));
            return false;
        }

        if (!super.init())
        {
            console.error(log.errInit('this'));
            return false;
        }

        return true;
    }

    protected destroy(): void
    {
        const log: kwLog = new kwLog(this.sClass, 'destroy');
        //console.log(log.called());

        super.destroy();
    }

    protected createPage(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, 'createPage');
        //console.log(log.called());

        if (kw.isNull(this.type))
        {
            console.error(log.invalid('type'));
            return false;
        }

        if (kw.isNull(this.srvcFltr))
        {
            console.error(log.invalid('srvcFltr'));
            return false;
        }

        this.page = new kwPageFltrSt(this.type, this.srvcFltr);
        if (!this.page.init())
        {
            console.error(log.errInit('page'));
            return false;
        }

        return true;
    }

}
*/
