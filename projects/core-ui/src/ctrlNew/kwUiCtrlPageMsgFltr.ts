/**********************************************************************
 *
 * kwUi/ctrl/kwUiCtrlPage.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
/*
import {kw}                         from '@kunstwerk/core';
import {kwUiCtrlPageMsg}            from '../ctrl/kwUiCtrlPageMsg';
import {kwFctyMsg}                  from '@kunstwerk/core';
import {kwLog}                      from '@kunstwerk/core';
import {kwStArr}                    from '@kunstwerk/core';
import {kwStObj}                    from '@kunstwerk/core';
import {kwStInit}                   from '../stat/kwUiStInit';
import {kwStMsg}                    from '@kunstwerk/core';
import {kwStVal}                    from '@kunstwerk/core';
import {kwStView}                   from '../stat/kwUiStView';


export abstract class kwUiCtrlPageMsgFltr extends kwUiCtrlPageMsg
{



    protected constructor(
        srvcAttrs: kwStVal,
        srvcDisp: kwStObj,
        srvcInit: kwStInit,
        srvcView: kwStView,
        srvcData: kwStArr,
        srvcFcty: kwFctyMsg,
        srvcMsg:  kwStMsg   )
    {
        super(
            srvcAttrs,
            srvcDisp,
            srvcInit,
            srvcView,
            srvcData,
            srvcFcty,
            srvcMsg    );

        const log: kwLog = new kwLog(this.sClass, 'constructor');
        //console.log(log.called());
    }


//@formatter:on

    protected init(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, 'init');
        //console.log(log.called());

        if (!super.init())
        {
            console.error(log.errInit('this'));
            return false;
        }

        return true;
    }

}
*/
