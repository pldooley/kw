import { NgModule } from '@angular/core';
import { AgGridComponent } from './ag-grid.component';

@NgModule({
  declarations: [AgGridComponent],
  imports: [
  ],
  exports: [AgGridComponent]
})
export class AgGridModule { }
